package no.experis.contactList.models;

public class Number {
    private Integer id;
    private String number;
    private String label;

    public Number(Integer id, String number, String label) {
        this.id = id;
        this.number = number;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public Integer getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }
}
