package no.experis.contactList.models;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Contact {
    private Integer id;
    private String firstName;
    private String lastName;
    private String dob;
    private String address;
    private String gender;
    private Integer parentIdOne;
    private Integer parentIdTwo;
    private ArrayList<Email> emailList = new ArrayList<Email>();
    private ArrayList<Number> numberList = new ArrayList<Number>();

    public Contact() {
    }

    public Contact(Integer id, String firstName, String lastName, String dob, String address, Integer parentIdOne, Integer parentIdTwo, String gender) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.address = address;
        this.parentIdOne = parentIdOne;
        this.parentIdTwo = parentIdTwo;
        this.gender = gender;
    }

    public Contact(Integer id, String firstName, String lastName, String dob, String address, Integer parentIdOne, String gender) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.address = address;
        this.parentIdOne = parentIdOne;
        this.gender = gender;
    }

    public Contact(Integer id, String firstName, String lastName, String dob, String address, String gender) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.address = address;
        this.gender = gender;
    }



    public Integer getId() {
        return id;
    }

    public Integer getParentIdOne() {
        return parentIdOne;
    }

    public String getAddress() {
        return address;
    }

    public Integer getParentIdTwo() {
        return parentIdTwo;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getDob() {
        return dob;
    }

    public String getGender() {
        return gender;
    }

    public String getLastName() {
        return lastName;
    }

    public ArrayList<Email> getEmailList() {
        return emailList;
    }

    public void setEmailList(String emailList, String label) {
        this.emailList.add(new Email(this.id, emailList, label));
    }

    public ArrayList<Number> getNumberList() {
        return numberList;
    }

    public void setNumberList(String numberList, String label) {
        this.numberList.add(new Number(this.id, numberList, label));
    }
}
