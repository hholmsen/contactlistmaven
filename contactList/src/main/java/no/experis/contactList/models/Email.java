package no.experis.contactList.models;

import java.util.ArrayList;
import java.util.HashMap;

public class Email {
    private Integer id;
    private String emailAddress;
    private String label;

    public Email() {}

    public Email(Integer id, String emailAddress, String label) {
        this.id = id;
        this.emailAddress = emailAddress;
        this.label = label;
    }

    public Integer getId() {
        return id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getLabel() {
        return label;
    }
}
