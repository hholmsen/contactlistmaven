package no.experis.contactList;

import no.experis.contactList.models.Contact;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContactListApplication {

	//public static Contact person = new Contact(1, "nicolas", "saint", "12/03/1995", "northpole69", "male");

	public static void main(String[] args) {



		SpringApplication.run(ContactListApplication.class, args);

	}

}
