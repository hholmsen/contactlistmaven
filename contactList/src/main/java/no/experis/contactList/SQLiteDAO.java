package no.experis.contactList;

import no.experis.contactList.models.Contact;
import no.experis.contactList.models.Email;
import no.experis.contactList.models.Number;


import java.sql.*;
import java.util.ArrayList;
import java.util.Random;

public class SQLiteDAO {
    private Connection conn;

    public SQLiteDAO() throws SQLException {
        this.conn = connect();

        String personSql = "CREATE TABLE if not exists Person" +
                "(Id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "Gender VARCHAR(255)," +
                "FirstName VARCHAR(255)," +
                "LastName VARCHAR(255)," +
                "DOB VARCHAR(255)," +
                "Address VARCHAR(255)," +
                "ParentID1 INTEGER, " +
                "ParentID2 INTEGER);";

        String emailAddressSql = "CREATE TABLE if not exists Email" +
                "(Id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "ContactID INTEGER," +
                "Email VARCHAR(255)," +
                "Label VARCHAR(255)," +
                "FOREIGN KEY(ContactID) REFERENCES Person(Id));";

        String contactNumberSql = "CREATE TABLE if not exists Number" +
                "(Id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "ContactID INTEGER," +
                "Number VARCHAR(255)," +
                "NumLabel VARVHAR(255)," +
                "FOREIGN KEY(ContactID) REFERENCES Person(Id));";

        String dropPersonTables = "DROP TABLE IF EXISTS Person;";
        String dropEmailTables = "DROP TABLE IF EXISTS Email;";
        String dropNumberTables = "DROP TABLE IF EXISTS Number;";

        Statement stmt = this.conn.createStatement();

        //dropping all tables
        stmt.execute(dropPersonTables);
        stmt.execute(dropEmailTables);
        stmt.execute(dropNumberTables);

        //Creating new Tables
        stmt.execute(personSql);
        stmt.execute(emailAddressSql);
        stmt.execute(contactNumberSql);


        //adding dummy data
        creatingDummyData();

    }


    public ArrayList<Contact> getSiblings(Contact child) {
        ArrayList<Contact> siblings = new ArrayList<>();
        String selectSql = "";

        if (getParents(child).size() == 2) { //If both parents are listed in the database
            selectSql = "SELECT * FROM Person WHERE (parentID1 = '" + child.getParentIdOne() + "' AND parentID2 = '" + child.getParentIdTwo() + "') " +
                    "OR (parentID1 = '" + child.getParentIdTwo() + "' AND parentID2 = '" + child.getParentIdOne() + "')";
        }

        if (selectSql.isEmpty()) {
            System.out.println("No get siblings select query is performed, both parents are not listed in the database");
        } else {
            try {
                Statement stmt = this.conn.createStatement();
                ResultSet rs = stmt.executeQuery(selectSql);
                while (rs.next()) {
                    if ((rs.getInt("Id")) != child.getId()) { //Makes sure current child is not being added as its own sibling
                        siblings.add(new Contact(rs.getInt("Id"), rs.getString("FirstName"), rs.getString("LastName"), rs.getString("DOB"), rs.getString("address"), rs.getInt("parentID1"), rs.getInt("parentID2"), rs.getString("Gender")));
                    }
                }

                return siblings;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return siblings;
    }


    public ArrayList<Contact> getParents(Contact child) {
        ArrayList<Contact> parents = new ArrayList<>();
        String selectSql = "";

        int firstParentId = child.getParentIdOne();
        int secondParentId = child.getParentIdTwo();

        if (firstParentId < 1 && secondParentId <1) {
            System.out.println("No parents in database");
        } else if (firstParentId < 1 && secondParentId > 1){
            selectSql =  "SELECT * FROM Person WHERE id = '"+secondParentId+"'";
        } else if (firstParentId > 1 && secondParentId < 1){
            selectSql =  "SELECT * FROM Person WHERE id = '"+firstParentId+"'";
        } else {
            selectSql = "SELECT * FROM Person WHERE id = '" + firstParentId + "' OR id = '" + secondParentId +"'";;
        }


        if (selectSql.isEmpty()) {
            System.out.println("No get parents select query is performed");
        } else {
            try {
                Statement stmt = this.conn.createStatement();
                ResultSet rs = stmt.executeQuery(selectSql);
                while (rs.next()) {
                    parents.add(new Contact(rs.getInt("Id"), rs.getString("FirstName"), rs.getString("LastName"), rs.getString("DOB"), rs.getString("address"), rs.getInt("parentID1"), rs.getInt("parentID2"), rs.getString("Gender")));
                }
                return parents;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return parents;
    }

    public void setParents(int childId, String parentOneId, String parentTwoId){
        try {
            updateContact(childId, "Person", "ParentID1", parentOneId+"", "Id");
            updateContact(childId, "Person", "ParentID2", parentTwoId + "", "Id");
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    // Brukes av add parent i add contact contact controller
    public Contact searchForContact(Contact parent){
        String selectSql = "SELECT * FROM Person WHERE firstname = '"+ parent.getFirstName() +"' AND lastName = '"+ parent.getLastName() +"' AND DOB = '"+ parent.getDob() +"'";

        try{
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(selectSql);
            return new Contact(rs.getInt("Id"), rs.getString("FirstName"), rs.getString("LastName"), rs.getString("DOB"), rs.getString("address"), rs.getInt("parentID1"), rs.getInt("parentID2"), rs.getString("Gender"));
        } catch(SQLException e){
            e.printStackTrace();
        }
        return null;
    }

    public void creatingDummyData() {
        String firstName[] = {"Nicolas", "Bror", "Helene", "Henrik", "Magnus", "Erik", "Eirik", "Sebastian", "Alan", "Bryan", "Johanna", "Brian", "Diego", "Ivan", "Jordan", "Kaden", "Karina", "Lilly", "Joan"};
        String lastName[] = {"Hansen", "Johansen", "Olsen", "Larsen", "Andersen", "Kristiansen", "Jesen", "Karlsen", "Johansen", "Pettersen", "Eriksen", "Berg", "Dahl"};
        String gender[] = {"male", "female"};
        String dob[] = {"010100", "230394", "230590", "160786", "141192"};
        String address[] = {"Nordsjø Veien 48", "Søris Gaten 12", "Rostedsgate 6b", "Skjalgsbakken 16", "Gabriel Tischendorfsvei 40", "Nordnes"};
        String[] email = {"email@gmail.com", "email@hotmail.com", "email@no.experis.no", "email@msn.no", "email@outlook.com"};
        String[] labels = {"work", "Home", "secondWife", "Girlfriend", "OnlyForCuddles", "Mistress"};
        String[] numbers = {"123456789", "3456789", "0987632", "234567"};
        Random rnd = new Random();


        for (int i = 0; i < 20; i++) {
            int r = rnd.nextInt(firstName.length);
            String theFirstName = firstName[rnd.nextInt(firstName.length)];
            String theLastName = lastName[rnd.nextInt(lastName.length)];
            Contact contactList = new Contact(null, theFirstName, theLastName, dob[rnd.nextInt(dob.length)], address[rnd.nextInt(address.length)], gender[rnd.nextInt(gender.length)]);

            for (int j = 0; j < rnd.nextInt(5); j++) {
                contactList.setEmailList(email[rnd.nextInt(email.length)], labels[rnd.nextInt(labels.length)]);
            }
            for (int j = 0; j < rnd.nextInt(5); j++) {
                contactList.setNumberList(numbers[rnd.nextInt(numbers.length)], labels[rnd.nextInt(labels.length)]);
            }


            insertFromContact(contactList);

        }
    }

    public Integer getIdFromContact(Contact contact) {
        String findString = "SELECT * from Person WHERE firstname=? AND lastname=? AND DOB=? AND  Address=? AND Gender=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(findString);
            stmt.setString(1, contact.getFirstName());
            stmt.setString(2, contact.getLastName());
            stmt.setString(3, contact.getDob());
            stmt.setString(4, contact.getAddress());
            stmt.setString(5, contact.getGender());
            try {
                ResultSet rs = stmt.executeQuery();
                return rs.getInt("Id");
            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    //Funker med string-key og string-value (dvs ikke id-values osv),
    // kjøres per nå med "http://localhost:8080/contactsby"
    public ArrayList<Contact> getContactsBy(String key, String value) {
        ArrayList<Contact> resultContacts = new ArrayList<>();

        String selectPerson = "SELECT * FROM Person WHERE " + key + " = '" + value + "'";


        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(selectPerson);
            while (rs.next()) {
                Contact contact = new Contact(rs.getInt("Id"), rs.getString("FirstName"), rs.getString("LastName"), rs.getString("DOB"), rs.getString("address"), rs.getInt("parentID1"), rs.getInt("parentID2"), rs.getString("Gender"));
                resultContacts.add(contact);
                resultContacts.addAll(getParents(contact));
                resultContacts.addAll(getSiblings(contact));
            }

            return resultContacts;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<Contact> getAllContacts() throws SQLException {
        ArrayList<Contact> contactList = new ArrayList<Contact>();
        //Todo rremove duplicates
        String selectPersons = "SELECT * FROM Person " +
                "LEFT JOIN Email ON Person.id=Email.ContactID " +
                "LEFT JOIN Number ON Person.id=Number.ContactID;";

        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(selectPersons);
            Contact contact = new Contact();
            Integer id = 0;
            while (rs.next()) {

                if (id < rs.getInt("Id") && contact.getId() != null) {
                    contactList.add(contact);
                    id = rs.getInt("Id");
                }
                contact = getContact(rs, contact);
            }
            contactList.add(contact);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contactList;
    }

    private Contact getContact(ResultSet rs, Contact contact) throws SQLException {
        if (contact.getId() == null || contact.getId() != rs.getInt("Id")) {
            contact = new Contact(rs.getInt("Id"), rs.getString("FirstName"), rs.getString("LastName"), rs.getString("DOB"), rs.getString("address"), rs.getInt("parentID1"), rs.getInt("parentID2"), rs.getString("Gender"));
        }

        if (rs.getString("Email") != null) {
            boolean newMail = true;
            for (Email e: contact.getEmailList()) {
                if(e.getEmailAddress().equals(rs.getString("Email")) && e.getLabel().equals(rs.getString("Label"))){
                    newMail = false;
                }
            }
            if(newMail)
            contact.setEmailList(rs.getString("Email"), rs.getString("Label"));

        }
        if (rs.getString("Number") != null) {
            boolean newNumber = true;
            for(Number n : contact.getNumberList()){
                if(n.getNumber().equals(rs.getString("Number")) && n.getLabel().equals(rs.getString("NumLabel"))){
                    newNumber = false;
                }
            }
            if(newNumber)
            contact.setNumberList(rs.getString("Number"), rs.getString("NumLabel"));
        }
        return contact;
    }


    public Contact getContactById(String id) throws SQLException {

        String selectPerson = "SELECT * FROM Person " +
                "LEFT JOIN Email ON Person.id=Email.ContactID " +
                "LEFT JOIN Number ON Person.id=Number.ContactID WHERE Person.Id ="+ id +" ;";

        try {
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery(selectPerson);

            return getContact(rs, new Contact());


        }catch (SQLException e){
            e.printStackTrace();
        }

        return null;

    }

    public void updateContact(Integer id, String table, String labelForUpdate, String newValue, String tableToChange) throws SQLException {
        String updateString = "UPDATE " + table + " SET " + labelForUpdate + " = " + newValue + " WHERE " + tableToChange + "=" + id + ";";

        PreparedStatement pstmt = this.conn.prepareStatement(updateString);


        boolean autoCommit = conn.getAutoCommit();
        try {
            conn.setAutoCommit(false);
            pstmt.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            conn.rollback();
        } finally {
            conn.setAutoCommit(autoCommit);
        }
    }


    public boolean delete(String id) {
        //Todo not yet tested if numbers get deleted if no emails.
        Integer ID = null;
        try {
            ID = Integer.parseInt(id);
        } catch (NumberFormatException e) {
            System.out.println("Illegal argument, id needs to be a number");
            e.printStackTrace();
            return false;
       }

        if (ID != null) {
            String[] sqlDeleteQuery = new String[]{"DELETE FROM Person WHERE " + ID + "=Id;", "DELETE FROM Email WHERE " + ID + "=Id;", "DELETE FROM Number WHERE " + ID + "=Id;"};

            try {
                    Statement stmt = this.conn.createStatement();
                    stmt.executeQuery(sqlDeleteQuery[0]);
            } catch (SQLException e) {
            }
            try {
                Statement stmt = this.conn.createStatement();
                stmt.executeQuery(sqlDeleteQuery[1]);
            } catch (SQLException e) {

            } try {
                Statement stmt = this.conn.createStatement();
                stmt.executeQuery(sqlDeleteQuery[2]);
            } catch (SQLException e) {
            }
            try {
                updateContact(ID, "Person", "ParentID1", null, "ParentID1");
            } catch (SQLException e) {
            }
            try {
                updateContact(ID, "Person", "ParentID2", null, "ParentID2");
            } catch (SQLException e) {
            }
            return true;
        }

        return false;
    }

    public boolean insert(String gender, String firstName, String lastName, String DOB, String address, Integer parentID1, Integer parentID2) throws SQLException {
        boolean success = true;
        String insertintoPerson = "INSERT INTO Person (Gender, FirstName, LastName, DOB, Address, ParentID1, ParentID2) " +
                "VALUES ('" + gender + "', '" + firstName + "', '" + lastName + "', '" + DOB + "', '" + address + "', '" + parentID1 + "', '" + parentID2 + "');";
        PreparedStatement preparedStatement = conn.prepareStatement(insertintoPerson);

        boolean autoCommit = conn.getAutoCommit();
        try {
            conn.setAutoCommit(false);
            preparedStatement.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            success = false;
            conn.rollback();
            e.printStackTrace();
        } finally {
            conn.setAutoCommit(autoCommit);
        }
        return success;
    }

    public void insertFromContact(Contact contact) {
        boolean autoCommit = false;

        String insertintoPerson = "INSERT INTO Person " +
                "(Gender, FirstName, LastName, DOB, Address, ParentID1, ParentID2) " + "VALUES (" +
                "'" + contact.getGender() +
                "', '" + contact.getFirstName() +
                "', '" + contact.getLastName() +
                "', '" + contact.getDob() +
                "', '" + contact.getAddress() +
                "', '" + contact.getParentIdOne() +
                "', '" + contact.getParentIdTwo() +
                "'" + ");";

        try {
            PreparedStatement preparedStatement = this.conn.prepareStatement(insertintoPerson);
            autoCommit = this.conn.getAutoCommit();
            this.conn.setAutoCommit(false);
            preparedStatement.executeUpdate();
            this.conn.commit();
        } catch (SQLException e) {
            try {
                this.conn.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                Integer id = getIdFromContact(contact);

                if (contact.getEmailList().size() > 0) {
                    for (int i = 0; i < contact.getEmailList().size(); i++) {
                        insertIntoEmail(contact.getEmailList().get(i), id);
                    }
                }

                if(contact.getNumberList().size() > 0) {
                    for (int i = 0; i < contact.getNumberList().size(); i++) {
                        insertIntoNumber(contact.getNumberList().get(i), id);
                    }
                }

                this.conn.setAutoCommit(autoCommit);


            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertIntoEmail(Email email, Integer contactId) {
        boolean autoCommit = false;

        String insertIntoEmail = "INSERT INTO Email " + "(ContactId, Email, Label)" + "VALUES (" +
                "'" + contactId +
                "', '" + email.getEmailAddress() +
                "', '" + email.getLabel() +
                "'" + ");";

        try {
            PreparedStatement preparedStatement = this.conn.prepareStatement(insertIntoEmail);
            autoCommit = this.conn.getAutoCommit();
            this.conn.setAutoCommit(false);
            preparedStatement.executeUpdate();
            this.conn.commit();
        } catch (SQLException e) {
            try {
                this.conn.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                this.conn.setAutoCommit(autoCommit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertIntoNumber(Number number, Integer contactId) {
        boolean autoCommit = false;

        String insertIntoEmail = "INSERT INTO Number " + "(ContactId, Number, NumLabel)" + "VALUES (" +
                "'" + contactId +
                "', '" + number.getNumber() +
                "', '" + number.getLabel() +
                "'" + ");";

        try {
            PreparedStatement preparedStatement = this.conn.prepareStatement(insertIntoEmail);
            autoCommit = this.conn.getAutoCommit();
            this.conn.setAutoCommit(false);
            preparedStatement.executeUpdate();
            this.conn.commit();
        } catch (SQLException e) {
            try {
                this.conn.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            e.printStackTrace();
        } finally {
            try {
                this.conn.setAutoCommit(autoCommit);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static Connection connect() {
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:ContactList.db";
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

}
