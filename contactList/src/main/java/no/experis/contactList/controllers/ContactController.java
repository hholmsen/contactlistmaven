package no.experis.contactList.controllers;

import no.experis.contactList.SQLiteDAO;
import no.experis.contactList.models.Contact;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.sql.SQLException;
import java.util.ArrayList;

@RestController
public class ContactController {

    SQLiteDAO dao;

    {
        try {
            dao = new SQLiteDAO();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @GetMapping("/contact/{id}")
    @ResponseBody
    public Contact contactFind(@PathVariable String id) throws SQLException {
        return dao.getContactById(id);
    }

    @RequestMapping("/contacts")
    public ArrayList<Contact> allContacts() throws SQLException {
        return dao.getAllContacts();
    }

    @PostMapping("/new")
    public Contact newContact(
            @RequestBody Contact contact) throws SQLException {
        System.out.println("---- NEW CONTACT -----");
        dao.insertFromContact(contact);
        return contact;
    }

    @GetMapping("/delete/{id}")
    @ResponseBody
    public RedirectView delete(@PathVariable String id){
        dao.delete(id);
        return new RedirectView("/");
    }


    @PostMapping(value ="/newcontact",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            headers="Accept=application/x-www-form-urlencoded")
    public RedirectView newContact(@RequestParam("firstName") String firstname, @RequestParam("lastName") String lastname,
                                   @RequestParam("d") String d, @RequestParam("o") String o, @RequestParam("b") String b,
                                   @RequestParam("address") String address, @RequestParam("gender") String gender,
                                   @RequestParam("email") String email, @RequestParam("emailtype") String emailtype,
                                   @RequestParam("phonenr") String number, @RequestParam("phonetype") String phonetype,@RequestParam("addparent") String addparent,
                                   @RequestParam("parentfFirstName") String parentFirstName,
                                   @RequestParam("parentLastName") String parentLastName,
                                   @RequestParam("parentDay") String parentDay, @RequestParam("parentMonth") String parentMonth, @RequestParam("parentYear") String parentYear,
                                   @RequestParam("parentGender") String parentGender,
                                   @RequestParam("secondParentFirstName") String secondParentFirstName,
                                   @RequestParam("secondParentLastName") String secondParentLastName,
                                   @RequestParam("secondParentDay") String secondParentDay, @RequestParam("secondParentMonth") String secondParentMonth, @RequestParam("secondParentYear") String secondParentYear,
                                   @RequestParam("secondParentGender") String secondParentGender) throws SQLException {
        System.out.println("---- NEW CONTACT -----");
        Contact child = new Contact(null, firstname, lastname, new String( d  +"/"+ o +"/"+ b ), address,gender);
        child.setEmailList(email, emailtype);
        child.setNumberList(number, phonetype);
        dao.insertFromContact(child);

        int childId = dao.getIdFromContact(child);

        if (addparent.equals("true")){
            Contact firstParent = new Contact(null, parentFirstName, parentLastName, new String( parentDay  + parentMonth + parentYear ) , "", parentGender);
            Contact secondParent = new Contact(null, secondParentFirstName, secondParentLastName, new String( secondParentDay  + secondParentMonth + secondParentYear ) , "", secondParentGender);

            Contact p1 = dao.searchForContact(firstParent);
            Contact p2 = dao.searchForContact(secondParent);

            String p1id = "";
            String p2id = "";

            if (p1 == null && p2 == null){  //Hvis ingen av foreldrene finnes i databasen
                dao.insertFromContact(firstParent); //legg til i databasen
                dao.insertFromContact(secondParent); //legg til i databasen
                p1id = dao.getIdFromContact(firstParent)+"";
                p2id = dao.getIdFromContact(secondParent)+"";
            } else if (p1 != null && p2 == null) { //Hvis bare p1 finnes i databasen
                dao.insertFromContact(secondParent);
                p1id = dao.getIdFromContact(p1)+"";
                p2id = dao.getIdFromContact(secondParent)+"";
            } else if (p1 == null && p2 != null) { //Hvis bare p2 finnes i databasen
                dao.insertFromContact(firstParent);
                p1id = dao.getIdFromContact(firstParent)+"";
                p2id = dao.getIdFromContact(p2)+"";
            } else { //Beggee er i datbasen
                p1id = dao.getIdFromContact(p1)+"";
                p2id = dao.getIdFromContact(p2)+"";
            }
            dao.setParents(childId, p1id, p2id);
        }
        return new RedirectView("/");
    }

    @RequestMapping( value ="/contactsBy",
            consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)

    public ArrayList<Contact> contactsBy(@RequestParam("firstName") String firstname) throws SQLException {

        return dao.getContactsBy("firstName" , firstname);
    }

    @GetMapping("/relationscontact/{id}")
    @ResponseBody
    public ArrayList<Contact> contactFindrelation(@PathVariable String id) throws SQLException {
        return dao.getContactsBy("id", id);
    }



}
